import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MdButtonModule, MdCheckboxModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {BasicComponent} from './calculators/basic/basic.component';
import {EngineeringComponent} from './calculators/engineering/engineering.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicComponent,
    EngineeringComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MdButtonModule,
    MdCheckboxModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {
        path: 'basic',
        component: BasicComponent
      }, {
        path: 'engineering',
        component: EngineeringComponent
      }
    ])
  ],
  exports: [MdButtonModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
