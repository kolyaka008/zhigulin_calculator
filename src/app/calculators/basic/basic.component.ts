import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  expression = '';
  onSelect(num: any): void {
    switch (num) {
      case '=': this.expression = '' + eval(this.expression); break;
      case 'c': this.expression = this.expression.slice(0, -1); break;
      case 'ce': this.expression = ''; break;
      default: this.expression += num; break;
    }
  }

}
