import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-engineering',
  templateUrl: './engineering.component.html',
  styleUrls: ['./engineering.component.css']
})
export class EngineeringComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  expression = '';
  // expression = '(5+3^2)^2-9/cos(sin(tan(52^2+log(10)+36)))'; // ~3471.5416
  // expression = 'sin(cos(tan(log(sqrt(4)))))^2'; // ~0.6244
  // expression = '2+(2+(2-2))*2-2*(2+2)+2*4'; // 6
  countTab = 0;
  point = false;

  onGlobalSelect(key: any): any {
    let numReg = /\d/g;
    if (numReg.test(key.key)) {
      this.onSelect(key.key);
    } else {
      switch (key.key) {
        case '+':
          this.onSelect('+');
          break;
        case '-':
          this.onSelect('-');
          break;
        case '*':
          this.onSelect('*');
          break;
        case '/':
          this.onSelect('/');
          break;
        case '(':
          this.onSelect('(');
          break;
        case ')':
          this.onSelect(')');
          break;
        case '=':
          this.onSelect('=');
          break;
        case '.':
          this.onSelect('.');
          break;
        case 'Enter':
          this.onSelect('=');
          break;
        case 'Delete':
          this.onSelect('c');
          break;
        case 's':
          this.onSelect('sin');
          break;
        case 'c':
          this.onSelect('cos');
          break;
        case 't':
          this.onSelect('tan');
          break;
        case 'q':
          this.onSelect('sqrt');
          break;
        case 'l':
          this.onSelect('log');
          break;
        case '^':
          this.onSelect('^2');
          break;
      }
    }
  }

  onSelect(num: any): void {
    switch (num) {
      case '=':
        if (!this.countTab) {
          this.expression = '' + this.parseArr(this.parseSum(this.expression));
        } else {
          this.countTab--;
          this.expression += ')';
          this.countTab ? this.onSelect('=') : this.expression = '' + this.parseArr(this.parseSum(this.expression));
        }
        break;
      case 'c':
        if (['(', ')'].indexOf(this.expression.slice(-1)) !== -1) {
          this.expression.slice(-1) === '(' ? this.countTab-- : this.countTab++;
        }
        this.expression = this.expression.slice(0, -1);
        break;
      case 'ce':
        this.expression = '';
        break;
      case 'log':
      case 'sqrt':
      case 'sin':
      case 'cos':
      case 'tan':
        this.countTab++;
        this.point = false;
        this.expression += num + '(';
        break;
      case '(':
        this.countTab++;
        this.point = false;
        this.expression += num;
        break;
      case ')':
        this.countTab--;
        this.point = false;
        this.expression += num;
        break;
      case '.':
        !this.point ? this.expression += num : false;
        this.point = true;
        break;
      case '^2':
      case '+':
      case '-':
      case '*':
      case '/':
        this.point = false;
        this.expression += num;
        break;
      default:
        this.expression += num;
        break;
    }
  }

  calculateEng(type, exp): number {
    exp += '';
    const pow = exp.indexOf('^');
    if (pow > -1) {
      return Math[type](eval(this.calcPow(exp, pow)));
    } else {
      return Math[type](eval(exp));
    }
  }

  parseSum(exp: any): string[] {
    this.countTab = 0;
    const temp = [];
    let count = 0;
    const resp = {
      last: 0,
      middle: 0
    };
    exp.split('').forEach((i, ind) => {
      if (i === '(') {
        count++;
      }
      if (i === ')') {
        count--;
      }
      if (!count && (i === '+' || i === '-')) {
        resp.last = ind;
        temp.push(resp.middle ? exp.slice(resp.middle, resp.last) : exp.slice(0, resp.last));
        temp.push(exp[resp.last]);
        resp.middle = resp.last + 1;
      }
      if (exp.length === ind + 1) {
        temp.push(exp.slice(resp.last ? resp.last + 1 : resp.last));
      }
    });
    return temp;
  }

  calcPow(item: string, pow: number): any {
    let count = pow;
    let temp = '';
    while (count >= 0) {
      if (item[pow - 1] === ')') {
        if (item[count] === '(') {
          temp = count === 0 ? temp : item.slice(0, count);
          temp += Math.pow(eval(item.slice(count, pow)), 2);
          temp += item.slice(pow + 2);
          return temp;
        }
      } else {
        if (count === 0 || ['+', '-', '*', '/'].indexOf(item[count]) > -1) {
          temp = count === 0 ? temp : item.slice(0, count);
          temp += Math.pow(eval(item.slice(count, pow)), 2);
          temp += item.slice(pow + 2);
          return temp;
        }
      }
      count--;
    }
  }

  parseArr(exp: string[]): any {
    exp = exp.map((item, ind) => {
      if ((ind + 1) % 2) {
        const log = this.isHaveLogFunc(item);
        if (log['ind'] > -1) {
          let temp = '';
          const lastLog = this.getLogStr(item, log['ind']);
          temp = log['ind'] > 0 ? item.slice(0, log['ind']) : temp;
          temp += this.calculateEng(log['log'],
            item.slice(log['log'] === 'sqrt' ? log['ind'] + 5 : log['ind'] + 4, lastLog - 1));
          temp = lastLog === item.length ? temp : temp + item.slice(lastLog);
          return temp;
        } else {
          const pow = item.indexOf('^');
          if (pow > -1) {
            return this.calcPow(item, pow);
          } else {
            return '' + eval(item);
          }
        }
      } else {
        return item;
      }
    });
    const temp = this.isHaveLogFunc(exp.join(''));
    if (temp['ind'] > -1 || exp.join('').indexOf('^') > -1) {
      return this.parseArr(exp);
    } else {
      return eval(exp.join(''));
    }
  }

  getLogStr(exp: string, start: number): any {
    let count = 0;
    let ind = 0;
    let i = start;
    while (i < exp.length) {
      if (!count && exp[i] === '(') {
        count++;
      }
      if (count && exp[i] === ')') {
        count--;
        if (count === 0) {
          ind = i;
          break;
        }
      }
      i++;
    }
    return ind + 1;
  }

  count = 0;

  isHaveLogFunc(exp: string): any {
    let isLog = {log: '', ind: -1};
    isLog = exp.indexOf('sin') !== -1 && isLog.ind < exp.indexOf('sin') ? {log: 'sin', ind: exp.indexOf('sin')} : isLog;
    isLog = exp.indexOf('cos') !== -1 && isLog.ind < exp.indexOf('cos') ? {log: 'cos', ind: exp.indexOf('cos')} : isLog;
    isLog = exp.indexOf('tan') !== -1 && isLog.ind < exp.indexOf('tan') ? {log: 'tan', ind: exp.indexOf('tan')} : isLog;
    isLog = exp.indexOf('log') !== -1 && isLog.ind < exp.indexOf('log') ? {log: 'log', ind: exp.indexOf('log')} : isLog;
    isLog = exp.indexOf('sqrt') !== -1 && isLog.ind < exp.indexOf('sqrt') ? {
      log: 'sqrt',
      ind: exp.indexOf('sqrt')
    } : isLog;
    return isLog;
  }
}

